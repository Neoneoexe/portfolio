---
date: '2'
title: 'AbdiuAuto'
cover: './card.png'
external: 'https://www.abdiuauto.ch'
tech:
  - Gatsby
  - Contentful
  - React
  - Vanilla Extract
  - Redux Toolkit
  - Framer Motion
  - Sharp
  - Styled Components
  - React Helmet
  - Bootstrap Icons
---

AbdiuAuto is a Swiss dealership and auto transport website built using Gatsby for fast static generation and Contentful as the content management system. The website integrates modern technologies like Redux Toolkit for state management, Framer Motion for animations, and Styled Components for styling. This project also utilizes Vanilla Extract for CSS management and Sharp for image processing, ensuring optimal performance and a smooth user experience.
