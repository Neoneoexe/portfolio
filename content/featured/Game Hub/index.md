---
date: '3'
title: 'Game Hub'
cover: './demo.png'
github: 'https://github.com/AlbinXXX/final'
external: 'https://final-murex-phi.vercel.app'
tech:
  - React
  - Zustand
  - Typescript
  - Axios
  - Framer Motion
  - Tanstack Query
  - Chakra UI
---

Game-Hub - a passion project and one of my initial forays into React development. It's a testament to my journey, showcasing that everyone starts somewhere. Dive in, explore, and witness the beginning of my coding adventure.
