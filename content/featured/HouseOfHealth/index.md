---
date: '1'
title: 'House Of Health - Kicevo'
cover: './card.png'
tech:
  - Next.js
  - Prismic
  - React
  - Tailwind CSS
  - Framer Motion
  - GSAP
  - Anime.js
  - Relume UI
  - Typescript
  - Swiper
---

House of Health is a government hospital website for the North Macedonian Hospital in Kichevo, built with Next.js and Prismic. It will feature public health records, news, and doctor publications. The content is being finalized, with the site launching soon. I was selected through a rigorous interview process to design, develop, and maintain the website, ensuring a professional and user-centered experience. Key technologies include Tailwind CSS for design, Framer Motion and GSAP for animations, and TypeScript for robust development. Swiper and Slick enhance user interaction through dynamic sliders.
