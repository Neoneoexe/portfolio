---
date: '2021-2023'
title: 'Full-Stack Software Engineer'
company: '💼 100Devs'
location: 'California, LA'
range: '2021 - 2023'
url: 'https://100devs.org'
---

- Contributed to a dynamic team in crafting cutting-edge, user-friendly e-commerce platforms, fostering seamless online shopping experiences.
- Engineered semantically rich, full-stack web applications, ensuring robust structures and smooth functionality for forward shipping systems.
- Implemented agile methodologies such as SCRUM to efficiently manage projects, maintaining focus and adaptability throughout the development process.
