---
date: '08-30-2022'
title: 'Engineering Solutions'
company: '🏆TEKNOFEST'
location: 'Turkey'
range: '2022'
url: 'https://www.teknofest.org/en/'
---

- Collaborated with teammates to create a social networking app, focusing on anonymous networking features.
- Developed a basic social networking application, emphasizing anonymous networking capabilities.
- Participated in building a simple social networking platform, with a focus on enabling anonymous interactions.
