---
date: '08-20-2024'
title: 'Project Manager & Sales Manager'
company: '💼 PWORLD'
location: 'Toronto, Canada 🇨🇦'
range: '2024-02 to 2024-08'
url: 'https://www.thepworld.com/'
---

- 🗂️ Managed and organized HR and PR events across North America, ensuring successful execution and delegate engagement.
- 📞 Led communication and outreach efforts with delegates, contributing to project success.
- 🎟️ Achieved double the ticket sales for the US market compared to anyone else in the office, driving significant revenue growth.
- 🛠️ Developed and implemented internal tools, including a lead-generation tool that increased daily new leads per salesperson by up to 1000%, from 200 to 2000 leads daily.
