---
date: '2023-current'
title: 'Bachelors of Computer Science'
company: '🎓 Education (SEEU)'
location: 'California, LA'
range: '2023 - current'
url: 'https://www.seeu.edu.mk'
---

- Hey there!
- Currently enrolled at SEEU.
