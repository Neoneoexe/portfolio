import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

// Import translation files
import enTranslation from './locales/en/translation.json';
import deTranslation from './locales/de/translation.json';

i18n
  .use(LanguageDetector) // Detects language
  .use(initReactI18next) // Passes i18n down to react-i18next
  .init({
    resources: {
      en: { translation: enTranslation },
      de: { translation: deTranslation },
    },
    fallbackLng: 'en', // Use English as the default language
    debug: true, // Enable debugging (you can disable it in production)
    detection: {
      order: ['localStorage', 'cookie', 'navigator', 'htmlTag', 'path', 'subdomain'],
      caches: ['localStorage'], // Cache the user's language in localStorage
    },
    interpolation: {
      escapeValue: false, // React already escapes from XSS
    },
  });

export default i18n;
