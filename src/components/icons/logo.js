import React from 'react';
import logo from '../../images/logo.svg';

const IconLogo = () => <img src={logo} alt="Logo" />;

export default IconLogo;
